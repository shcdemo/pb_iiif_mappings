# README #

This repository contains files to map pb elements of Early Print texts to IIIF urls pointing to an image
for each page. Currently all images mapped here are served from the Internet Archive, but any images
with open access licenses that are available on the Internet via the IIIF protocol are fair game.

### Contents and Description ###

There is one CSV file for each discrete XML text in Early Print for which we have matched images
from the Internet Archive. Some other texts have facsimile sections that are not generated from
these data and point elsewhere for their images.

* Filenames follow the pattern "%\_iiif.csv" where % is the number of the SHC filename
* Column 1 of each csv is the content of the xml:id for a pb element
* Column 2 is a IIIF url to an image of that page
	
### Contribution guidelines ###

This repository was set up by Kate Needham, who directed the mapping of a number files by
summer interns at Northwestern (2016 and 2017).  Martin Mueller contributed many additional
mappings and Craig Berry developed the tools for integrating those mappings into the texts.
If you have more mappings, do send us a pull request or just get them to us in whatever form
is convenient.

### Who do I talk to? ###

Write the [editors](mailto:editors@earlyprint.org) about any problems with files, how they are created,
what they mean, etc.

### Tools ###

The `util/` directory contains a number of tools for manipulating the mapping data, converting it into
facsimile sections, and incorporating those sections into the Eearly Print XML documents.

On Unix-like systems, you can regenerate and replace all the facsimile sections for which we have
data using the following command:

```
$ ls -1 *iiif.csv | perl util/pb2url_parseonly.pl ../eebotcp/texts | xargs -n1 util/splice.sh
```

This assumes the text repository is at `../eebotcp/texts`, that Perl is available with the
`XML::Parser` module installed, and the Xalan XSLT processor is installed.

Witness notes indicating the source of each images set are produced produced by the script
`util/get_image_witnesses.pl` using local data from `early_print_images_metadata.tab` and
retrieving other data from the metadata available at the Internet Archive.  The results are
stored in the file `witness_notes.txt`.  This file is in turn used by `util/pb2url_parseonly.pl`
when producing facsimile sections.