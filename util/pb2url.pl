use strict;
use XML::Parser;

# This program expects to find comma-separated CSV files in the current directory that
# contain mappings of the xml:id for <pb> elements to URLs.  It loads up a hash with
# those mappings, and then looks in the directory supplied as a command-line argument
# for XML files in which to insert <facsimile> elements based on the mappings.  The
# <facsimile> element is inserted immediately after <teiHeader> and any existing
# <facsimile> element is discarded.

# To generate the facsimile section, we need the "facs" attributes from all the <pb>
# elements in the document.  In order to avoid parsing the document twice, we process
# up through the end of teiHeader and flush the output, then process the rest of the
# document, buffering it all in memory.  Finally, we generate the facsimile section
# based on the mappings and the facs attributes and print it out, and then flush the
# remainder of the document.

# Sample usages.
#  All files in the directory:
#    $ ls -1 *_iiif.csv | perl util/pb2url.pl ../phase1texts/texts
#
#  All files in the most recent commit:
#    $ git diff --name-only HEAD~1 *.csv | perl util/pb2url.pl ../phase1texts/texts

my $docdata = '';
my %pbid2url;
my %textids;
my %pbid2facs;
my $in_facsimile = 0;
my %tcpid2note;

my $start_element_actions = {
    'pb'         => \&start_element_pb
};

my $end_element_actions = {
    'teiHeader'  => \&end_element_teiHeader,
    'facsimile'  => \&end_element_facsimile
};

my $xmldir = $ARGV[0];

while (my $mapfile = <STDIN>) {
    chomp $mapfile;
    print "Processing $mapfile\n";
    if ($mapfile eq 'A10231_Purchas_iiif.csv') {
        print "Skipping $mapfile: incompatible format\n";
        next;
    }
    open my $fh, '<', $mapfile or die "Could not open $mapfile: $!";
    while (my $line = <$fh>) {
        chomp $line;
        my @line = split /,\s*/, $line;
        my $pbid = $line[0];
        $pbid =~ s/^\s+|\s+$//g;
        next unless length($pbid);
        my $url = $line[1];
        $url =~ s/^\s+|\s+$//g;
        next unless length($url);
        next if $url eq 'NA';
        unless ($url =~ m/\$(\d+)$/) {
            warn "page $pbid has no digits at end of URL: $url";
            next;
        }
        my $textid = $pbid;
        $textid =~ s/-.*//;
        $textids{$textid}++;
        $pbid2url{$textid}{$pbid} = $url;
    }
    close $fh;
}

# Slurp in the file containing witness notes.

open my $witfh, '<', 'witness_notes.txt' or die "Could not open 'witness_notes.txt: $!";

while (my $line = <$witfh>) {
    chomp $line;
    my ($text, $note) = split /\t/, $line;
    $tcpid2note{$text} = $note;
}
close $witfh;

my $p = new XML::Parser(Handlers => {Start   => \&handle_start,
                                     End     => \&handle_end,
                                     Char    => \&handle_char,
                                     Default => \&handle_default
                                    },
                        ProtocolEncoding => 'UTF-8');

my $group_count;
my $text_count;
my ($file, $outfh, $outfile);

for my $text (sort keys %textids) {
    my $file = $xmldir . '/' . substr($text, 0, 3) . "/${text}.xml";
    printf STDERR "Parsing $file\n";
    my $outfile = "$file.new";
    open $outfh, ">:utf8", $outfile or die "Couldn't open $outfile $!";
    $p->parsefile($file);
    print $outfh facsimile($text);
    flush_cdata();
    close $outfh;

    rename $outfile, $file or die "Couldn't rename $outfile to $file";
    
}


exit;


sub handle_start {
    return if $in_facsimile;
    my @attribute_names = ();
    my %attribute_values = ();
    my $self = shift;
    my $elnam = shift;
    
    if ($elnam eq 'facsimile') {
        $in_facsimile = 1;
        return;
    }

    while (my $attr = shift) {
        push @attribute_names, $attr;
        $attribute_values{$attr} = shift;
        $attribute_values{$attr} =~ s/\&/&amp;/g;
        $attribute_values{$attr} =~ s/\"/&quot;/g;
#        $attribute_values{$attr} =~ s/\'/&apos;/g;
        $attribute_values{$attr} =~ s/\</&lt;/g;
        $attribute_values{$attr} =~ s/\>/&gt;/g;

    }
  
    $docdata .= "<$elnam";
  
    for my $attr (@attribute_names) {
       $docdata .= " $attr=\"$attribute_values{$attr}\"";
    }
    $docdata .= ">";

    my $action = $start_element_actions->{$elnam};
    if ($action) {
        $action->($self, $elnam, \%attribute_values);
    }
}

sub handle_end {
    my $self = shift;
    my $elnam = shift;
    $docdata .= "<\/$elnam>" unless $in_facsimile;

    my $action = $end_element_actions->{$elnam};
    if ($action) {
        $action->($self, $elnam);
    }
}

sub end_element_facsimile {
    $in_facsimile = 0;
}

sub start_element_pb {
    my ($self, $elnam, $attrs) = @_;
    
    my $type = $$attrs{'type'};
    return if (defined($type) && $type eq 'blank');
    $pbid2facs{$$attrs{'xml:id'}} = $$attrs{'facs'};

}

sub end_element_teiHeader {
    flush_cdata();
}

sub handle_char {
    return if $in_facsimile;
    my $self = shift;
    my $string = shift;

    $string =~ s/\&/&amp;/g;
    $string =~ s/</&lt;/g;
    $string =~ s/>/&gt;/g;
    $docdata .= $string;
}

sub handle_default {
    return if $in_facsimile;
    my ($self, $string) = @_;
    $docdata .= $string;
}

sub flush_cdata {
    print $outfh $docdata if length($docdata);
    $docdata = '';
}

sub facsimile {
    my $textid = shift;
    my $facsimile = <<"HEADER";

 <facsimile xmlns:eml="http://earlyprint.org/ns/1.0">
  <!--
   This facsimile section includes URLs for <graphic> elements that have been
   automatically generated by a stylesheet. The URLs may be changed as necessary to point to
   corresponding page images, but the surface xml:ids should not change so long as they
   correctly account for the page sequence of the book.
  -->
  <surfaceGrp n="lab">
HEADER

    $facsimile .= "    <note type=\"witnessDetail\">$tcpid2note{$textid}</note>\n";

    for my $pbid (sort keys %{$pbid2url{$textid}}) {
        next unless exists $pbid2facs{$pbid} and exists $pbid2url{$textid}{$pbid};
        my $url = $pbid2url{$textid}{$pbid};
        my $n = $pbid2facs{$pbid};

        $facsimile .= <<"SURFACE";
   <surface corresp="#$n" xml:id="${pbid}-im">
    <graphic url="$url"/>
   </surface>
SURFACE
    }

    $facsimile .= <<"EEBOSECTION";
  </surfaceGrp>
  <surfaceGrp n="eebo">
EEBOSECTION
    for my $pbid (sort keys %{$pbid2url{$textid}}) {
        next unless exists $pbid2facs{$pbid} and exists $pbid2url{$textid}{$pbid};
        my $n = $pbid2facs{$pbid};
        $facsimile .= qq|   <surface corresp="#$n" n="$n"/>\n|;
    }

    $facsimile .= '  </surfaceGrp>
 </facsimile>';

    return $facsimile;
}