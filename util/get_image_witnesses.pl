use strict;
use warnings;

use LWP::Simple;
use LWP::UserAgent;
use JSON qw( decode_json );

# This program expects to find comma-separated CSV files in the current directory that
# contain mappings of the xml:id for <pb> elements to URLs.  It derives the URL for
# the Internet Archive metadata from the image URL and retrieves the witness information
# from that metadata.

# Sample usages.
#  All files in the directory:
#    $ ls -1 *_iiif.csv | perl util/get_image_witnesses.pl > witness_notes.txt
#
#  All files in the most recent commit:
#    $ git diff --name-only HEAD~1 *.csv | perl util/get_image_witnesses.pl >> witness_notes.txt

my %tcpid2url;
my %tcpid2note;
my %tcpid2manual_notes;

my $text;

while (my $mapfile = <STDIN>) {
    chomp $mapfile;
    if ($mapfile eq 'A10231_Purchas_iiif.csv') {
        printf STDERR "Skipping $mapfile: incompatible format\n";
        next;
    }
    open my $fh, '<', $mapfile or die "Could not open $mapfile: $!";
    my ($textid, $text);
    while (my $line = <$fh>) {
        chomp $line;
        my @line = split /,\s*/, $line;
        next if scalar @line == 0;

        $textid = $line[0];
        $textid =~ s/^\s+|\s+$//g;
        $textid =~ s/-.*//;
        next unless length($textid);

        my $url = $line[1];
        $url =~ s/^\s+|\s+$//g;
        next unless length($url);
        next if $url eq 'NA';
        $text = '';
        if ($url =~ m/archive\.org\/iiif\/(.*)\$/) {
            $text = $1;
            last;
        }
    }
    close $fh;
    unless (length($textid)) {
        warn "Hmm. textid: $textid, text: $text: mapfile: $mapfile";
        next;
    }

    $tcpid2url{$textid} = "https://archive.org/metadata/$text/";
}


my $first_time = 1;

open my $fh, '<', 'early_print_images_metadata.tab' or die "Could not open 'early_print_images_metadata.tab': $!";
while (my $line = <$fh>) {
    if ($first_time) {
        $first_time = 0;
        next;
    }
    chomp $line;
    my @columns = split /\t/, $line;
    next unless defined $columns[0] && length $columns[0];

    map { $_ =~ s/(^[\s\"]+|[\s\"]+$)//g } @columns;
    my $tcp_id = $columns[2];
#    my $source = $columns[8];
    my $contributor = $columns[12];
    my $call_no = $columns[13];
    my $type = $columns[14];
    my $facs_year = $columns[16];
#    my $notes = $columns[17];
    
    my $witness_note = "$contributor\t";
    $witness_note .= ", $call_no" if length $call_no;
    if ($type eq 'later facs') {
        $witness_note .= "; $type";
        $witness_note .= " $facs_year" if defined $facs_year and length($facs_year);
    }
    $tcpid2manual_notes{$tcp_id} = $witness_note;
}
close $fh;

# This one just has two columns, TCP ID and the pre-assembled note that goes with it.
open $fh, '<', 'simple_witness_notes.tab' or die "Could not open 'simple_witness_notes.tab': $!";
while (my $line = <$fh>) {
    chomp $line;
    my ($tcp_id, $witness_note) = split /\t/, $line;
    $tcpid2manual_notes{$tcp_id} = $witness_note;
}
close $fh;

for my $textid (sort keys %tcpid2url) {
    printf STDERR "Processing $textid\n";

    my $manual_note = $tcpid2manual_notes{$textid} || "\t";
    my ($contributor, $note) = split /\t/, $manual_note;
    $note //= '';
    my $source = source_from_url($tcpid2url{$textid}, $contributor);
    $note .= ", " if length $contributor && length $source;
    my $witness_note = $contributor . $note . $source;
    next unless length $witness_note;

    print "$textid\t$witness_note\n";
}


exit;

sub source_from_url {

    my $url = shift;
    my $spreadsheet_contributor = shift;
    my $json;

    return '' unless length $url;

    my $ua = LWP::UserAgent->new( ssl_opts => { verify_hostname => 0 } );
    my $response = $ua->get($url);

    if ( $response->is_success ) {
        $json = $response->decoded_content;
    }
    else {
        die "For $url: " . $response->status_line;
    }
    
    my $decoded_json = decode_json( $json );
    my $contributor =  $decoded_json->{'metadata'}{'contributor'} || '';
    my $call_no = $decoded_json->{'metadata'}{'call_number'} || '';
    my $witness = $contributor eq $spreadsheet_contributor ? '' : $contributor;

    $witness .= ', ' if length($witness) && length($call_no);
    $witness .= $call_no if length $call_no;
    return $witness;
}
