use strict;

my %textids;

my $xmldir = $ARGV[0];

while (my $mapfile = <STDIN>) {
    chomp $mapfile;
    print "Processing $mapfile\n";
    if ($mapfile eq 'A10231_Purchas_iiif.csv') {
        print "Skipping $mapfile: incompatible format\n";
        next;
    }
    open my $fh, '<', $mapfile or die "Could not open $mapfile: $!";
    while (my $line = <$fh>) {
        chomp $line;
        my @line = split /,\s*/, $line;
        my $pbid = $line[0];
        $pbid =~ s/^\s+|\s+$//g;
        next unless length($pbid);
        my $url = $line[1];
        $url =~ s/^\s+|\s+$//g;
        next unless length($url);
        next if $url eq 'NA';
        unless ($url =~ m/\$(\d+)$/) {
            warn "page $pbid has no digits at end of URL: $url";
            next;
        }
        my $textid = $pbid;
        $textid =~ s/-.*//;
        $textids{$textid}++;
    }
    close $fh;
}

print "Missing files:\n";
for my $text (sort keys %textids) {
    my $file = $xmldir . '/' . substr($text, 0, 3) . "/${text}.xml";
    print "$file\n" unless -f $file;
}


exit;

