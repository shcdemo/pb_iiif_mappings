<?xml version="1.0"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:tei="http://www.tei-c.org/ns/1.0">

  <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*"/>

<!--
    Example usage:

    xalan -i 1-o A11909_00.xml.new -p facs "'A11909_00.xml.facs'" A11909_00.xml splice.xsl  
-->

  <xsl:param name="facs"/>

  <xsl:template match="*">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@* | text() | comment() | processing-instruction()">
    <xsl:copy/>
  </xsl:template>

  <!-- insert facsimile section in top-level structure -->

  <xsl:template match="tei:TEI">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="tei:teiHeader"/>
      <xsl:apply-templates select="document($facs, .)/tei:TEI/tei:facsimile"/>
      <xsl:apply-templates select="tei:text"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>