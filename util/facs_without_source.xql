xquery version "3.1";
declare namespace tei="http://www.tei-c.org/ns/1.0";

let $source := '/db/apps/EarlyPrintTexts/texts/'

let $result :=
    for $match in collection($source)//tei:facsimile
    order by data($match/ancestor::tei:TEI/@xml:id)
    return
        if (count($match/tei:surfaceGrp/tei:note[@type="witnessDetail"]) = 0)
        then
            <li>{data($match/ancestor::tei:TEI/@xml:id)} (missing)</li>
        else
            if ($match/tei:surfaceGrp/tei:note[@type="witnessDetail"]/string() = '')
            then
                <li>{data($match/ancestor::tei:TEI/@xml:id)} (empty)</li>
            else
                ()

return $result


