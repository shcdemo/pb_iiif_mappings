#!/bin/sh

# Splice pre-generated facsimile section into document.
#
# Usage:   $ util/splice.sh ../phase1texts/texts/A36/A36708.xml

tmp="$(mktemp)"
doc="$1"
facs=$(basename "${1}.facs")

xalan -i 1 -o "$tmp" -p facs "'${facs}'" "${doc}" util/splice.xsl || { echo 'xalan: facsimile insertion failed for:' "$doc" >&2; exit 1; }

mv "$tmp" "${doc}"
rm "${doc}.facs"