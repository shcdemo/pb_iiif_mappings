<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs"
    version="3.0">

<!--
    Run with something like:

    $ java -jar ../SaxonHE11-4J/saxon-he-11.4.jar -xsl:util/harvest_facsimiles.xsl \
           -s:util/harvest_facsimiles.xsl \
           input-dir=~/repos/eebotcp/texts
           output-dir=./ 
-->

    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

    <xsl:param name="input-dir">../../eebotcp/texts</xsl:param>
    <xsl:param name="output-dir">./</xsl:param>

    <xsl:template match="/">
        <xsl:for-each select="collection(concat($input-dir, '?recurse=yes;select=*.xml'))">
            <xsl:variable name="code5full" select="/tei:TEI/@xml:id/data()"/>
            <xsl:variable name="tcpid" select="/tei:TEI/@xml:id/data()"/>
            <xsl:if test="//tei:facsimile">
                <xsl:variable name="outputpath" select="concat($output-dir, '/', $tcpid, '_iiif.csv')"/>
                <xsl:message select="concat(position(), '&#x09;', replace(base-uri(), '.*/', ''), '&#x09;', $outputpath)"></xsl:message>
                <xsl:result-document href="{$outputpath}">
                    <xsl:apply-templates/>
                </xsl:result-document>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="//tei:facsimile/tei:surfaceGrp/tei:surface[@xml:id]">
        <xsl:variable name="pbid" select="replace(@xml:id/data(), '-im', '')"/>
        <xsl:variable name="url" select="tei:graphic/@url[1]/data()"/>
        <xsl:value-of select="concat($pbid, ',', $url, '&#xa;')"/>
    </xsl:template>

    <!-- Identity -->
    <xsl:template match="*">
        <xsl:apply-templates select="@* | node()"/>
    </xsl:template>

    <xsl:template match="@* | text() | comment() | processing-instruction()"/>
</xsl:stylesheet>
