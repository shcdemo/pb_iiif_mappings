use strict;
use XML::Parser;

# This program expects to find comma-separated CSV files in the current directory that
# contain mappings of the xml:id for <pb> elements to URLs.  It loads up a hash with
# those mappings, and then looks in the directory supplied as a command-line argument
# for the corresponding XML file(s) that should already contain the corresponding
# <facsimile> section.  If the <facsimile> element is not found, a 

# Sample usages.
#  All files in the directory:
#    $ ls -1 *_iiif.csv | perl util/find_missing_facsimile_sections.pl ../eebotcp/texts
#
#  All files in the most recent commit:
#    $ git diff --name-only HEAD~1 *.csv | perl util/find_missing_facsimile_sections.pl ../eebotcp/texts

my %textids;
my $facsimile_found;

my $xmldir = $ARGV[0];

while (my $mapfile = <STDIN>) {
    chomp $mapfile;
    printf STDERR "Processing $mapfile\n";
    if ($mapfile eq 'A10231_Purchas_iiif.csv') {
        printf STDERR "Skipping $mapfile: incompatible format\n";
        next;
    }
    my $textid = $mapfile;
    $textid =~ s/_iiif\.csv$//;
    $textids{$textid} = $mapfile;
}

my $group_count;
my $text_count;
my ($file, $outfh, $outfile);

for my $text (sort keys %textids) {
    $facsimile_found = 0;
    my $file = $xmldir . '/' . substr($text, 0, 3) . "/${text}.xml";
    unless (-f $file) {
        warn "$file does not exist -- skipping";
        next;
    }
    my $p = new XML::Parser(Handlers => {Start   => \&handle_start,
                                        Default => \&handle_default
                                        },
                            ProtocolEncoding => 'UTF-8');
    $p->parsefile($file);
    $p = undef;

    if (!$facsimile_found) {
        print $textids{$text} . "\n";
    }
}


exit;

sub handle_start {
    my @attribute_names = ();
    my %attribute_values = ();
    my $self = shift;
    my $elnam = shift;

    if ($elnam eq 'text') {
        $self->finish();
        return;
    }

    if ($elnam eq 'facsimile') {
        $facsimile_found = 1;
        return;
    }
}

sub handle_default {
    return;
}
