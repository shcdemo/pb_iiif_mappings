use strict;

$| = 1;

while (my $file = glob('*.csv')) {
#    print "Checking file $file . . . ";
    my $counter = 0;
    open my $fh, '<', $file or die "Could not open $file: $!";
    while (my $line = <$fh>) {
        chomp $line;
        my @line = split /,\s*/, $line;
        my $url = $line[1];
        next unless length($line[0]);
        unless ($url =~ m/\$(\d+)$/) {
            print "page $line[0] has no digits at end of URL: $url\n";
            next;
        }
        my $image_num = $1;
        print "page $line[0]: discontinuity at image number $image_num (counter $counter).\n" if ($counter > 0 && $counter != $image_num);
        $counter = ++$image_num;
    }
    $counter--;
#    print "file $file has a last image of $counter\n";
    close $fh;
}