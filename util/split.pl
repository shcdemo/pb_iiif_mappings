#!/usr/bin/perl

my $file_to_split = $ARGV[0];
open my $in_fh, '<:utf8', $file_to_split or die "Couldn't open $file_to_split: $!";

my $file = '';
my $fh;

while (my $line = <$in_fh>) {
    $line =~ s/\s+//g;
    $line =~ m/^([A-C]\d{5})-/;

    if ($file ne "$1_iiif.csv") {
        $file = "$1_iiif.csv";
        close $fh if defined $fh;
        open $fh, '>:utf8', $file or die "Couldn't open output $file";
    }
    print $fh "$line\n";
}
close $fh;
close $in_fh;